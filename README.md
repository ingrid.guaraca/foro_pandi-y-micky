El programa realizado en mblock permite que el usuario o visitante, visualice 
a Pandi donde él saluda a su amigo Micky, ven un ovni, luego Micky le dice a 
Pandi que tiene operaciones para que los visitantes las realicen. Estas
operaciones son divisiones de números decimales con un entero, mismas deben ser
redondeadas a la milésima, y al terminar las operaciones se despiden y Pandi se 
va a descansar.

Este programa no puede ser ejecutado en Python o en su IDE PyCharm, ya que al 
momento de importarlo y quererlo ejecutar se presenta un error, pero hay que 
destacar que su funcionamiento lo podemos visualizar dentro de la plataforma 
mblock donde se ejecuta sin problema alguno.

Además hago constar el documento mblock original llamado pandi.mblock y el 
cambio que se le realizó consta en el documento PandiyMicky.mblock