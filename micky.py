import sprite

from mblock import event
import time
import random

@event.greenflag
def on_greenflag():
    sprite.x = 185
    sprite.y = -95


@event.received('mensaje1')
def on_received():
    sprite.show()
    sprite.say('¡Hola Pandi!', 1.5)
    for count in range(1):
        sprite.glide(87, -95, 1)

    sprite.say('Estoy muy bien', 1)
    sprite.broadcast(str('2'))
    time.sleep(5)


@event.received('2')
def on_received1():
    sprite.show()
    sprite.think('Mmm... y eso será un ovni?',2)
    time.sleep(1.5)
    sprite.say('Pandi, mira un ovni', 1)
    sprite.broadcast(str('3'))


@event.received('4')
def on_received2():
    sprite.say('Woo!!', 2)
    time.sleep(1)
    sprite.broadcast(str('ope'))


@event.received('go')
def on_received3():
    sprite.set_variable('Número', 0)
    sprite.set_variable('Puntuación', 0)
    sprite.say('Realiza la siguiente operación', 2)
    sprite.say('Redondea el resultado a la milésima', 3)
    sprite.say('Debes ingresar punto en vez de coma', 2)
    for count2 in range(5):
        time.sleep(1)
        sprite.set_variable('NumeroAleatorio', round((100 * random.uniform(0.01, 0.99))) / 100)
        sprite.set_variable('NumeroAleatorio2', random.randint(1, 100))
        v = sprite.get_variable('Número')
        sprite.set_variable('Número', v + 1)
        sprite.set_variable('Respuesta', sprite.get_variable('NumeroAleatorio') / sprite.get_variable('NumeroAleatorio2'))
        answer = sprite.input(str(str(str(str('(') + str(sprite.get_variable('NumeroAleatorio'))) + str(')')) + str('/')) + str(str(str('(') + str(sprite.get_variable('NumeroAleatorio2'))) + str(')')))
        if sprite.answer == round((1000 * sprite.get_variable('Respuesta'))) / 1000:
            sprite.say('¡Correcto!', 2)
            v = sprite.get_variable('Puntuación')
            sprite.set_variable('Puntuación', v + 1)

        else:
            sprite.say('Incorrecto', 2)


    sprite.say(str('Tu puntuación es de ') + str(sprite.get_variable('Puntuación')), 2)
    sprite.hide()
    sprite.broadcast(str('bye'))


@event.received('ope')
def on_received4():
    sprite.say('Bueno Pandi vamos hacer que nuestros visitantes realicen operaciones', 5)
    sprite.broadcast(str('lets'))