import sprite

from mblock import event
import time

@event.greenflag
def on_greenflag():
    sprite.hide()


@event.received('4')
def on_received():
    sprite.say('Woo!!', 2)
    time.sleep(1)
    sprite.broadcast(str('ope'))


@event.received('chao')
def on_received1():
    sprite.hide()


@event.received('bye')
def on_received2():
    sprite.show()
    sprite.say('Bueno esto fue todo.   Ádios, Pandi', 1)
    sprite.glide(-222, -95, 1)
    sprite.broadcast(str('5'))