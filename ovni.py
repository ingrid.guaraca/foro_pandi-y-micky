import sprite

from mblock import event

@event.received('ope')
def on_received():
    sprite.hide()


@event.greenflag
def on_greenflag():
    sprite.hide()


@event.received('2')
def on_received1():
    sprite.show()
    for count in range(2):
        sprite.next_costume()
        sprite.glide(119, 115, 1)
        sprite.next_costume()
        sprite.glide(-29, 117, 1)
        sprite.next_costume()
        sprite.glide(-108, 124, 1)
        sprite.next_costume()
        sprite.glide(-167, 123, 1)