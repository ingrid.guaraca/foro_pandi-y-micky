import sprite

from mblock import event
import time

@event.greenflag
def on_greenflag():
    sprite.x = -206
    sprite.y = -102
    sprite.show()
    for count in range(1):
        sprite.next_costume()
        sprite.glide(-141, -89, 1)
        sprite.next_costume()
        sprite.glide(-126, -86, 1)

    sprite.say('¡Hola Micky!', 1)
    sprite.broadcast(str('mensaje1'))
    sprite.say('¿Cómo estás?', 2)


@event.received('3')
def on_received():
    time.sleep(1)
    sprite.say('¡Si!, es un ovni!', 2)
    sprite.broadcast(str('4'))
    sprite.say('Woo!!', 2)


@event.received('5')
def on_received1():
    sprite.say('Ádios!', 2)
    sprite.broadcast(str('chao'))
    sprite.think('Me voy a dormir',2)
    time.sleep(0.5)
    for count2 in range(1):
        sprite.next_costume()
        sprite.glide(-33, -89, 1)
        sprite.next_costume()
        sprite.glide(30, -86, 1)

    sprite.broadcast(str('duerme'))


@event.received('duerme')
def on_received2():
    time.sleep(1)
    sprite.x = 2
    sprite.y = -18
    sprite.say('Buenas noches', 2)
    sprite.play('Zzzzzz')
    time.sleep(1.5)
    sprite.set_backdrop('pandidor')
    sprite.play_until_done('jack-in-the-box')


@event.backdrop_change('pandidor')
def on_backdrop_change():
    sprite.hide()


@event.received('lets')
def on_received3():
    sprite.say('¡Comencemos!', 2)
    sprite.broadcast(str('go'))